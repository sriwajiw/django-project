import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email:string;
  password:string;
  constructor(private router:Router, private authService:AuthService) { 
    if(localStorage.getItem('token')){
      this.router.navigateByUrl('/dashboard')
    }

  }

  ngOnInit() {
  }

  navigateToRegister(){
    this.router.navigateByUrl('/register')
  }

  login(){
    let data={
      email:this.email,
      password:this.password
    }
    this.authService.login(data).subscribe((datas:any)=>{
      if(datas){
      localStorage.setItem('token',datas.token);
      this.router.navigateByUrl('/dashboard');
    }else{
        console.log("Cannto login")
      }
    })
  }

}
