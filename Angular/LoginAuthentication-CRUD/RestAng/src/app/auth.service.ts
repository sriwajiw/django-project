import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
 });
options = { headers: this.headers };
  constructor(private http:HttpClient) { }

  BASE_URL ='https://reqres.in/api/';
  // BASE_URL ='http://127.0.0.1:8000/rest-auth/login/';

  login(data:{email:string,password:string}){
    return this.http.post(this.BASE_URL + 'login',data);
  }

  register(data:{email:string,password:string}):Observable<any>{
    console.log(JSON.stringify(data))
    return this.http.post<any>(this.BASE_URL + 'register',JSON.stringify(data),this.options);
  }


}
