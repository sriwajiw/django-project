import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
import { User } from '../user.model';

@Component({
  selector: 'app-users-create',
  templateUrl: './users-create.component.html',
  styleUrls: ['./users-create.component.scss']
})
export class UsersCreateComponent implements OnInit {
  first_name :String;
  last_name:String;
  email:String;
  address:String;
  phone:Number;

  data:any;
  constructor(private user:DataService) {
    // this.first_name="ds";
    // this.last_name="";
    // this.email='';
    // this.address="";
    // this.phone=16613;
   }

  ngOnInit() {
    
  }

  sendData(){
    this.data={
      first_name:this.first_name,
      last_name:this.last_name,
      email:this.email,
      address:this.address,
      phone:this.phone
    }
    console.log(this.data)
    this.user.postUser(this.data).subscribe(data=>{
      console.log(data);
    });
  }

}
