import { Component } from '@angular/core';
import { User } from './user.model';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  users : User[];
  
  constructor(private dataService: DataService) {}

  ngOnInit() {
    return this.dataService.getUsers().subscribe(res => { 
      console.log(res);
      this.users = res['results'];
    });
  }
}
