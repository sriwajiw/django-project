import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
email:string;
password:string;
data:any={}
  constructor(private authService:AuthService,private route:Router) { }

  ngOnInit() {
  }

  onRegister(){
    this.data={
      email:this.email,
      password:this.password
    }

    this.authService.register(this.data).subscribe(data=>{
      if(data){
        this.route.navigateByUrl('/login');
      }else{
        console.log("Register Unsuccessfull")
      }
    })
  }

  

}
