import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users : User[];
  
  constructor(private dataService: DataService,private router:Router) {}

  ngOnInit() {
    return this.dataService.getUsers().subscribe(res => {
      console.log(res);
      this.users = res['results'];
    });
  }

  logout(){
    localStorage.removeItem('token');
    this.router.navigateByUrl('/');
  }

}
