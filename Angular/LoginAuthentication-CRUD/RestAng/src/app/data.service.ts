import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user.model';
@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiUrl = 'http://127.0.0.1:8000/users/';
   
  constructor(private _http: HttpClient) { }


  getUsers() {
    return this._http.get<User[]>(this.apiUrl);
  }

  postUser(payload){
    return this._http.post<User>(this.apiUrl,payload)
    
  }
}
