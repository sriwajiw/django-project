from django.db import models

# Create your models here.
class UserModel(models.Model):
    uid = models.IntegerField()
    uemail = models.CharField(max_length=120)
    phone = models.IntegerField(null=True)

class ProfileModel(models.Model):
    pid = models.ForeignKey(UserModel,on_delete=models.CASCADE)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    address = models.CharField(max_length=120)