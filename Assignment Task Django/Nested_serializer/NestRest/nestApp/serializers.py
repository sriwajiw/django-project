from .models import UserModel, ProfileModel
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserModel
        fields = '__all__'


class ProfileSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ProfileModel
        fields = '__all__'
       


class UserProfileSerializer(serializers.ModelSerializer):
    pid = UserSerializer(read_only=True)
    class Meta:
        model = ProfileModel
        fields = '__all__'