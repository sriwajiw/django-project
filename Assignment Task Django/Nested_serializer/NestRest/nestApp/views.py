from django.shortcuts import render
from rest_framework import viewsets

from .models import UserModel, ProfileModel
from .serializers import UserSerializer, ProfileSerializer, UserProfileSerializer

class UserViewSet(viewsets.ModelViewSet):
    queryset = UserModel.objects.all()
    serializer_class = UserSerializer

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = ProfileModel.objects.all()
    serializer_class = ProfileSerializer

class UserProfileViewSet(viewsets.ModelViewSet):
    queryset = ProfileModel.objects.all()
    serializer_class = UserProfileSerializer