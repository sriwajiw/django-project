from django.shortcuts import render
from bookapp.forms import BookForm
# Create your views here.
from django.shortcuts import render, redirect, get_object_or_404
from .models import BookModel

def index(request):
    template = 'bookapp/list.html'
    book = BookModel.objects.all()
    context = {"book": book}
    return render(request, template, context)

def book_create(request):
    template = 'bookapp/form.html'
    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('/')
    context = {"form": form}
    return render(request, template, context) 

def book_update(request, pk):
    template = 'bookapp/form.html'
    book = get_object_or_404(BookModel, pk=pk)
    form = BookForm(request.POST or None, instance=book)
    if form.is_valid():
        form.save()
        return redirect('/')
    context = {"form": form}
    return render(request, template, context)


def book_delete(request, pk):
    template = 'bookapp/delete.html'
    book = get_object_or_404(BookModel, pk=pk)
    if request.method == 'POST':
        book.delete()
        return redirect('/')
    context = {"book": book}
    return render(request, template, context)