from django.db import models

# Create your models here.
class BookModel(models.Model):
    name = models.CharField(max_length=50)
    price = models.IntegerField(null=True)
    detail = models.TextField(null=True)

    def __str__(self):
        return self.name