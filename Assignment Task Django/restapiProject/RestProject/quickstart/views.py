from django.shortcuts import render
from django.contrib.auth.models import User, Group
from rest_framework import viewsets

from .models import Language
from .serializers import LangSerializer


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Language.objects.all()
    serializer_class = LangSerializer

