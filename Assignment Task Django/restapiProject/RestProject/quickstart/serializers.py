from .models import Language
from rest_framework import serializers


class LangSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ['name', 'paradigm']

