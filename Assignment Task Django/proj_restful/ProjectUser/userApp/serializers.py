from .models import UserModel
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = UserModel
        fields = '__all__'
        extra_kwargs = {
            'first_name': {'write_only': True},
            'last_name': {'write_only': True}
        }

    def get_name(self, user):
        return user.first_name + ' ' + user.last_name
