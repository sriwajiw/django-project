from django.db import models

# Create your models here.
class UserModel(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=120)
    address = models.CharField(max_length=120)
    phone = models.IntegerField(max_length=50, null=True)