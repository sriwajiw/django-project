from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect, get_object_or_404
from .models import AuthModel


def index(request):
    template = 'authapp/list.html'
    book = AuthModel.objects.all()
    context = {"book": book}
    return render(request, template, context)