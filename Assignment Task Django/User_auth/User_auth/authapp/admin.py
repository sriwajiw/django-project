from django.contrib import admin
from authapp.models import AuthModel

# Register your models here.
admin.site.register(AuthModel)